export function custom(flickity){

    var galleryElems = document.querySelectorAll('.main-carousel');

    setTimeout(function(){
        //[INICIO] Flickity 
        for ( var i=0, len = galleryElems.length; i < len; i++ ) {
            var galleryElem = galleryElems[i];
            var flk = new flickity( galleryElem, {
                cellAlign: 'center',
                contain: true,
                groupCells: true,
                lazyLoad: 1,
                fullscreen:true,
                //freeScroll: true,
                // wrapAround: true,
                watchCSS: true
            });
        }
        //[FIN] Flickity 
    }, 3000);

    $('[data-toggle="tooltip"]').tooltip();
}