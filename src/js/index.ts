import * as $ from 'jquery';
import 'bootstrap';
import 'daterangepicker';
import 'flickity';
import 'lightbox2';
import "lightgallery";
import "select2";
import Cookies from 'js-cookie';
var flickity = require('flickity');
var moment = require('moment');
import 'flickity-fullscreen';

/**
 * Styles
 */
import '../scss/index.scss';

/**
 * Modules
 */

var GoogleMapsLoader = require('google-maps'); // only for common js environments
GoogleMapsLoader.KEY = 'AIzaSyBYM6g1VQqizIr3K-sKM_STsZrQSjwL9K4';
GoogleMapsLoader.VERSION = '3.35';

import { custom } from './custom';
import { menuScroll } from './scroll';
import { maps } from './maps';
import { galeria } from './lightGallery';


$('#zonas').select2({
    closeOnSelect: true,
    selectOnClose: true
  });
$('#hoteles').select2({
    closeOnSelect: true,
    selectOnClose: true
});

Cookies.set('holiholi', 'holihola');
/**
 * Init
 */

custom(flickity);
menuScroll();
maps(GoogleMapsLoader);
galeria();