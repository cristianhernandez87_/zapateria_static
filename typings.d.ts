interface JQuery {
   daterangepicker(options?: any, callback?: Function) : any;
   select2(options?: any, callback?: Function) : any;
}